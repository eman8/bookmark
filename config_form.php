<fieldset id="fieldset-embed"><legend><?php echo __('Bookmarks parameters'); ?></legend>
    <div class="field">
        <label for="bookmarks_show_link"><?php echo __('Show link to all contents created by user'); ?></label>
        <div class="inputs">
            <input type="checkbox" class="checkbox" name="bookmarks_show_link" <?php get_option('bookmarks_show_link') ? $checked = 'checked' : $checked = ''; echo $checked; ?> id="bookmarks_show_link" />
        </div>
    </div>
</fieldset>
