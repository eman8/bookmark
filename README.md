Ce module propose diverses fonctionnalités destinées à avoir un espace de travail pour chaque personne ayant un mot de passe pour atteindre le tableau de bord. Les fonctionnalités proposées sont :

    une page de présentation des utilisateurs
    un système de favoris
    le recensement de tous les contenus créés par chaque utilisateur
    un système de notes privées


Une fois connecté, vous pouvez utiliser ces fonctionnalités dans la barre noire supérieure quand vous êtes sur l'interface publique (là où vous avez Administration d'Omeka, Se déconnecter, Editer ce contenu). On a fait le choix de ne pas rajouter dans le tableau de bord déjà saturé ces fonctionnalités qui concernent surtout vos propres activités.

Pour l'instant ces fonctionnalités sont réservées aux personnes ayant un compte sur votre site.

## Credits

Plugin Version 0.1 réalisé pour la plate-forme EMAN (ENS-CNRS-Sorbonne nouvelle) par Vincent Buard (Numerizen)avec le soutien du projet ANR Fiches de lecture Foucault : https://ffl.hypotheses.org. Voir les explications sur le site [EMAN](https://eman-archives.org/EMAN/bookmark)
